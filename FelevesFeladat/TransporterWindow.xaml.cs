﻿// <copyright file="TransporterWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for TransporterWindow.xaml
    /// </summary>
    public partial class TransporterWindow : Window
    {
        private Login logIn;

        private ILogic logic;

        private bool logout = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransporterWindow"/> class.
        /// </summary>
        /// <param name="username">Username of logged in</param>
        /// <param name="password">PW of logged in</param>
        public TransporterWindow(string username, string password)
        {
            this.InitializeComponent();
            this.InitializeComponent();
            this.logIn = new Login();
            this.logic = new Logic();

            var tempuser = this.logIn.LoginInto(username, password);
            TransporterWindowViewModel.Get.User = tempuser;
            this.DataContext = TransporterWindowViewModel.Get;

            this.Checking();
        }

        private void Btn_products_Click(object sender, RoutedEventArgs e)
        {
            TransporterWindowViewModel.Get.UpdateList();
            this.Datagrid.ItemsSource = StockwindowsViewModel.Get.Products;
            TransporterWindowViewModel.Get.Flag = "P";
            this.lbl_stat.Content = TransporterWindowViewModel.Get.Products.Count();
        }

        private void Btn_locations_Click(object sender, RoutedEventArgs e)
        {
            TransporterWindowViewModel.Get.UpdateList();
            this.Datagrid.ItemsSource = StockwindowsViewModel.Get.Places;
            TransporterWindowViewModel.Get.Flag = "L";
            this.lbl_stat.Content = TransporterWindowViewModel.Get.Places.Count();
        }

        private void Btn_new_product_Click(object sender, RoutedEventArgs e)
        {
            ProductsEditViewModel.Get.Flag = "R";
            ProductsEdit pe = new ProductsEdit();
            pe.ShowDialog();
        }

        private void Btn_edit_produckt_Click(object sender, RoutedEventArgs e)
        {
            if (TransporterWindowViewModel.Get.Selected != null && TransporterWindowViewModel.Get.Flag == "P")
            {
                string[] tmp = this.logic.Find(TransporterWindowViewModel.Get.Selected, "P");

                ProductsEditViewModel.Get.SelectedProductID = tmp[0];
                ProductsEditViewModel.Get.SelectedProductName = tmp[1];
                ProductsEditViewModel.Get.SelectedProductPrice = tmp[2];
                ProductsEditViewModel.Get.SelectedProductWarehouseID = tmp[3];
                ProductsEditViewModel.Get.SelectedProductDescription = tmp[4];
                ProductsEditViewModel.Get.Flag = "M";

                ProductsEdit pe = new ProductsEdit();
                pe.ShowDialog();
            }
            else if (TransporterWindowViewModel.Get.Flag == "L")
            {
                MessageBox.Show("A szállítónak nincsen joguk szerkeszteni a raktárak adatait!", "Figyelmeztetés", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
            else
            {
                MessageBox.Show("Jelöljön ki egy elemet a szerkesztéshez!", "Szerkesztés", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.logout)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void Btn_edit_account_Click(object sender, RoutedEventArgs e)
        {
            string[] tmp = this.logic.Find(TransporterWindowViewModel.Get.User, "U");

            UserAccountViewModel.Get.AccountDataSetup(tmp, "TM");
            UserAccount ua = new UserAccount();
            ua.ShowDialog();
        }

        private void Checking()
        {
            string warn = "Az alábbi termékek esetében szükséges lenne újabb beszállítás.";
            bool needwarn = false;
            foreach (var item in this.logic.GetTempProductsList())
            {
                if (item.Darabszam < 10)
                {
                    warn = warn + "\nTermékazonosító: " + item.Termekazon + " Név: " + item.Nev + " Darabszám" + item.Darabszam;
                    needwarn = true;
                }
            }

            if (needwarn == true)
            {
                MessageBox.Show(warn);
            }
        }

        private void Btn_logout_Click(object sender, RoutedEventArgs e)
        {
            this.logout = true;
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }
    }
}
