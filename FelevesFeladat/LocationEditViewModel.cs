﻿// <copyright file="LocationEditViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ViewModel for LocationEdit
    /// </summary>
    internal class LocationEditViewModel : Bindable
    {
        private static LocationEditViewModel vM;

        private LocationEditViewModel()
        {
        }

        /// <summary>
        /// Gets new instance of ViewModel
        /// </summary>
        public static LocationEditViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new LocationEditViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets warehouse id
        /// </summary>
        public string WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets zip code
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the person responsible for place
        /// </summary>
        public string Person { get; set; }

        /// <summary>
        /// Gets or sets the flag
        /// </summary>
        public string Flag { get; set; }
    }
}
