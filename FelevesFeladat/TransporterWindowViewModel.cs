﻿// <copyright file="TransporterWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BusinessLogic;

    /// <summary>
    /// ViewModel of TransporterWindow
    /// </summary>
    internal class TransporterWindowViewModel : Bindable
    {
        private static TransporterWindowViewModel vM;

        private ILogic logic = new Logic();

        private TransporterWindowViewModel()
        {
            this.UpdateList();
        }

        /// <summary>
        /// Gets viewmodel
        /// </summary>
        public static TransporterWindowViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new TransporterWindowViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the selected object
        /// </summary>
        public object Selected { get; set; }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public object User { get; set; }

        /// <summary>
        /// Gets or sets the product list
        /// </summary>
        public List<object> Products { get; set; }

        /// <summary>
        /// Gets or sets the places list
        /// </summary>
        public List<object> Places { get; set; }

        /// <summary>
        /// Gets or sets the flag
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// Updates flags
        /// </summary>
        public void UpdateList()
        {
            this.Products = this.logic.GetAllProducts().ToList();
            this.Places = this.logic.GetAllPlaces().ToList();
        }
    }
}
