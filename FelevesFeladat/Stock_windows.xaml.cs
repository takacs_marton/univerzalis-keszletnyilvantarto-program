﻿// <copyright file="Stock_windows.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for Stock_windows.xaml
    /// </summary>
    public partial class Stock_windows : Window
    {
        private Login logIn;
        private bool logout = false;
        private ILogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stock_windows"/> class.
        /// </summary>
        /// <param name="username">Username of logged in</param>
        /// <param name="password">PW of logged in</param>
        public Stock_windows(string username, string password)
        {
            this.InitializeComponent();
            this.logIn = new Login();
            this.logic = new Logic();

            var tempuser = this.logIn.LoginInto(username, password);
            this.DataContext = StockwindowsViewModel.Get;
            this.lbl_stat.Content = StockwindowsViewModel.Get.Users.Count();
            StockwindowsViewModel.Get.Flag = "U";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.logout)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void Btn_account_list_Click(object sender, RoutedEventArgs e)
        {
            StockwindowsViewModel.Get.UpdateList();
            this.Datagrid.ItemsSource = StockwindowsViewModel.Get.Users;
            this.lbl_stat.Content = StockwindowsViewModel.Get.Users.Count();
            StockwindowsViewModel.Get.Flag = "U";
        }

        private void Btn_products_Click(object sender, RoutedEventArgs e)
        {
            StockwindowsViewModel.Get.UpdateList();
            this.Datagrid.ItemsSource = StockwindowsViewModel.Get.Products;
            this.lbl_stat.Content = StockwindowsViewModel.Get.Products.Count();
            StockwindowsViewModel.Get.Flag = "P";
        }

        private void Btn_edit_acount_Click(object sender, RoutedEventArgs e)
        {
            if (StockwindowsViewModel.Get.Selected != null)
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                UserAccountViewModel.Get.AccountDataSetup(tmp, "M");
                UserAccountViewModel.Get.Flag = "M";

                UserAccount ua = new UserAccount();
                ua.ShowDialog();
            }
            else
            {
                UserAccountViewModel.Get.Flag = "R";
                UserAccount ua = new UserAccount();
                ua.ShowDialog();
            }
        }

        private void Btn_locations_Click(object sender, RoutedEventArgs e)
        {
            StockwindowsViewModel.Get.UpdateList();
            this.Datagrid.ItemsSource = StockwindowsViewModel.Get.Places;
            this.lbl_stat.Content = StockwindowsViewModel.Get.Places.Count();
            StockwindowsViewModel.Get.Flag = "L";
        }

        private void Btn_edit_product_Click(object sender, RoutedEventArgs e)
        {
            if (StockwindowsViewModel.Get.Selected != null)
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                ProductsEditViewModel.Get.SelectedProductID = tmp[0];
                ProductsEditViewModel.Get.SelectedProductName = tmp[1];
                ProductsEditViewModel.Get.SelectedProductPrice = tmp[2];
                ProductsEditViewModel.Get.SelectedProductWarehouseID = tmp[3];
                ProductsEditViewModel.Get.SelectedProductDescription = tmp[4];
                ProductsEditViewModel.Get.SelectedProductQuantity = tmp[5];
                ProductsEditViewModel.Get.Flag = "M";

                ProductsEdit pe = new ProductsEdit();
                pe.ShowDialog();
            }
            else
            {
                ProductsEditViewModel.Get.Flag = "R";
                ProductsEdit pe = new ProductsEdit();
                pe.ShowDialog();
            }
        }

        private void Btn_edit_location_Click(object sender, RoutedEventArgs e)
        {
            if (StockwindowsViewModel.Get.Selected != null)
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                LocationEditViewModel.Get.WarehouseID = tmp[0];
                LocationEditViewModel.Get.City = tmp[1];
                LocationEditViewModel.Get.Street = tmp[2];
                LocationEditViewModel.Get.Zip = tmp[3];
                LocationEditViewModel.Get.Email = tmp[4];
                LocationEditViewModel.Get.Flag = "M";

                LocationEdit le = new LocationEdit();
                le.ShowDialog();
            }
            else
            {
                LocationEditViewModel.Get.Flag = "R";
                LocationEdit le = new LocationEdit();
                le.ShowDialog();
            }
        }

        private void Btn_delete_Click(object sender, RoutedEventArgs e)
        {
            if (StockwindowsViewModel.Get.Selected != null)
            {
                StockwindowsViewModel.Get.DeleteSelected();
            }
            else
            {
                MessageBox.Show("Válasszon ki egy törlendő elemet!");
            }

            if (StockwindowsViewModel.Get.Flag == "U")
            {
                this.Btn_account_list_Click(null, null);
            }
            else if (StockwindowsViewModel.Get.Flag == "P")
            {
                this.Btn_products_Click(null, null);
            }
            else if (StockwindowsViewModel.Get.Flag == "L")
            {
                this.Btn_locations_Click(null, null);
            }

            this.Datagrid.UnselectAll();
            this.Datagrid.Items.Refresh();
        }

        private void Btn_modify_Click(object sender, RoutedEventArgs e)
        {
            if (StockwindowsViewModel.Get.Selected != null && StockwindowsViewModel.Get.Flag == "L")
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                LocationEditViewModel.Get.WarehouseID = tmp[0];
                LocationEditViewModel.Get.City = tmp[1];
                LocationEditViewModel.Get.Street = tmp[2];
                LocationEditViewModel.Get.Zip = tmp[3];
                LocationEditViewModel.Get.Email = tmp[4];
                LocationEditViewModel.Get.Flag = "M";

                LocationEdit le = new LocationEdit();
                le.ShowDialog();
            }
            else if (StockwindowsViewModel.Get.Selected != null && StockwindowsViewModel.Get.Flag == "P")
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                ProductsEditViewModel.Get.SelectedProductID = tmp[0];
                ProductsEditViewModel.Get.SelectedProductName = tmp[1];
                ProductsEditViewModel.Get.SelectedProductPrice = tmp[2];
                ProductsEditViewModel.Get.SelectedProductWarehouseID = tmp[3];
                ProductsEditViewModel.Get.SelectedProductDescription = tmp[4];
                ProductsEditViewModel.Get.SelectedProductQuantity = tmp[5];
                ProductsEditViewModel.Get.Flag = "M";

                ProductsEdit pe = new ProductsEdit();
                pe.ShowDialog();
            }
            else if (StockwindowsViewModel.Get.Selected != null && StockwindowsViewModel.Get.Flag == "U")
            {
                string[] tmp = this.logic.Find(StockwindowsViewModel.Get.Selected, StockwindowsViewModel.Get.Flag);

                UserAccountViewModel.Get.AccountDataSetup(tmp, "M");
                UserAccountViewModel.Get.Flag = "M";

                UserAccount ua = new UserAccount();
                ua.ShowDialog();
            }
            else
            {
                MessageBox.Show("Válasszon ki egy módosítandó elemet!");
            }

            this.Datagrid.Items.Refresh();
        }

        private void Btn_new_Click(object sender, RoutedEventArgs e)
        {
                if (StockwindowsViewModel.Get.Flag == "P")
                {
                    ProductsEditViewModel.Get.Flag = "R";
                    ProductsEdit pe = new ProductsEdit();
                    pe.ShowDialog();
                    this.Btn_products_Click(null, null);
                }
                else if (StockwindowsViewModel.Get.Flag == "L")
                {
                    LocationEditViewModel.Get.Flag = "R";
                    LocationEdit le = new LocationEdit();
                    le.ShowDialog();
                    this.Btn_locations_Click(null, null);
                }
                else if (StockwindowsViewModel.Get.Flag == "U")
                {
                    UserAccountViewModel.Get.Flag = "R";
                    UserAccount ua = new UserAccount();
                    ua.ShowDialog();
                    this.Btn_account_list_Click(null, null);
                    this.Datagrid.UnselectAll();
            }

                this.Datagrid.Items.Refresh();
        }

        private void Btn_logout_Click(object sender, RoutedEventArgs e)
        {
            this.logout = true;
            MainWindow mw = new MainWindow();
            mw.Show();
            this.CloseWindow();
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void Btn_logs_Click(object sender, RoutedEventArgs e)
        {
            LogWindow lw = new LogWindow(this.logic);
            lw.ShowDialog();
        }
    }
}
