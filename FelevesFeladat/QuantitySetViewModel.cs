﻿// <copyright file="QuantitySetViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ViewModel for QuantitySet
    /// </summary>
    internal class QuantitySetViewModel : Bindable
    {
        private static QuantitySetViewModel vM;

        private QuantitySetViewModel()
        {
        }

        /// <summary>
        /// Gets the viewmodel
        /// </summary>
        public static QuantitySetViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new QuantitySetViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        public int SelectedQuantity { get; set; }
    }
}
