﻿// <copyright file="ProductsEdit.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for ProductsEdit.xaml
    /// </summary>
    public partial class ProductsEdit : Window
    {
        private ILogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsEdit"/> class.
        /// </summary>
        public ProductsEdit()
        {
            this.InitializeComponent();
            this.logic = new Logic();

            if (StockwindowsViewModel.Get.Selected != null || TransporterWindowViewModel.Get.Selected != null)
            {
                this.DataContext = ProductsEditViewModel.Get;
            }

            this.KeyDown += this.ProductsEdit_KeyDown;
        }

        private void ProductsEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bool result = false;
                string[] tomb = new string[]
                {
                this.txt_pro_id.Text,
                this.txt_pro_name.Text,
                this.txt_pro_info.Text,
                this.txt_price_price.Text,
                this.txt_pro_in_stock.Text,
                this.txt_pro_darab.Text
                };

                try
                {
                    if (this.txt_pro_id.Text == string.Empty || this.txt_pro_name.Text == string.Empty || this.txt_pro_info.Text == string.Empty || this.txt_price_price.Text == string.Empty || this.txt_pro_in_stock.Text == string.Empty)
                    {
                        MessageBox.Show("Kérlek töltsd ki a mezőket!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        int azon = int.Parse(this.txt_pro_id.Text);
                        int ar = int.Parse(this.txt_price_price.Text);

                        result = this.logic.AddProduct(tomb);
                    }

                    if (ProductsEditViewModel.Get.Flag == "R")
                    {
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres termék hozzáadás", string.Empty);
                                });
                            });

                            MessageBox.Show("Sikeres hozzáadás.", "Hozzáadás", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            this.DialogResult = true;
                        }
                        else
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Hiba a hozzáadás során", string.Empty);
                                });
                            });

                            MessageBox.Show("Hiba a hozzáadás során!\nEllenőrizze, hogy nem e foglalt termék azonosítót próbál használni.", "Hiba", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                    else if (ProductsEditViewModel.Get.Flag == "M")
                    {
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres módosítás", string.Empty);
                                });
                            });

                            MessageBox.Show("Sikeres módosítás.", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            this.DialogResult = true;
                        }
                        else if (result == false)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Hiba a módosítás során", string.Empty);
                                });
                            });

                            MessageBox.Show("Hiba a módosítás során!", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                catch (Exception h)
                {
                    Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.logic.LogCreator(h.Message, string.Empty);
                        });
                    });

                    MessageBox.Show("Hibás adatok!\nEllenőrizze, hogy a termék azonosítóhoz, az árhoz és a raktár azonosítóhoz jó értéket adott e meg!", "Hibás adatok", MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            }

            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void Btn_save_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            string[] tomb = new string[]
            {
                this.txt_pro_id.Text,
                this.txt_pro_name.Text,
                this.txt_pro_info.Text,
                this.txt_price_price.Text,
                this.txt_pro_in_stock.Text,
                this.txt_pro_darab.Text
            };

            try
            {
                if (this.txt_pro_id.Text == string.Empty || this.txt_pro_name.Text == string.Empty || this.txt_pro_info.Text == string.Empty || this.txt_price_price.Text == string.Empty || this.txt_pro_in_stock.Text == string.Empty)
                {
                    MessageBox.Show("Kérlek töltsd ki a mezőket!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    int azon = int.Parse(this.txt_pro_id.Text);
                    int ar = int.Parse(this.txt_price_price.Text);

                    result = this.logic.AddProduct(tomb);
                }

                if (ProductsEditViewModel.Get.Flag == "R")
                {
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Sikeres hozzáadás.", string.Empty);
                            });
                        });

                        MessageBox.Show("Sikeres hozzáadás.", "Hozzáadás", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        this.DialogResult = true;
                    }
                    else
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Hiba a hozzáadás során", string.Empty);
                            });
                        });

                        MessageBox.Show("Hiba a hozzáadás során!\nEllenőrizze, hogy nem e foglalt termék azonosítót próbál használni.", "Hiba", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                else if (ProductsEditViewModel.Get.Flag == "M")
                {
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Sikeres módosítás", string.Empty);
                            });
                        });

                        MessageBox.Show("Sikeres módosítás.", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        this.DialogResult = true;
                    }
                    else if (result == false)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Hiba a módosítás során", string.Empty);
                            });
                        });

                        MessageBox.Show("Hiba a módosítás során!", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception h)
            {
                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator(h.Message, string.Empty);
                    });
                });

                MessageBox.Show("Hibás adatok!\nEllenőrizze, hogy a termék azonosítóhoz, az árhoz és a raktár azonosítóhoz jó értéket adott e meg!", "Hibás adatok", MessageBoxButton.OK, MessageBoxImage.Stop);
            }

            // catch (InvalidOperationException ex)
            // {
            //    MessageBox.Show(ex.Message);
            // }
        }

        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
