﻿// <copyright file="ProductsEditViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ViewModel for ProductsEdit
    /// </summary>
    internal class ProductsEditViewModel : Bindable
    {
        private static ProductsEditViewModel vM;

        private ProductsEditViewModel()
        {
        }

        /// <summary>
        /// Gets ViewModel
        /// </summary>
        public static ProductsEditViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new ProductsEditViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the id
        /// </summary>
        public string SelectedProductID { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string SelectedProductName { get; set; }

        /// <summary>
        /// Gets or sets the price
        /// </summary>
        public string SelectedProductPrice { get; set; }

        /// <summary>
        /// Gets or sets the warehouse id
        /// </summary>
        public string SelectedProductWarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string SelectedProductDescription { get; set; }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        public string SelectedProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the flag
        /// </summary>
        public string Flag { get; set; }
    }
}
