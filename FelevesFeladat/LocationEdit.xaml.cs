﻿// <copyright file="LocationEdit.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for LocationEdit.xaml
    /// </summary>
    public partial class LocationEdit : Window
    {
        private ILogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationEdit"/> class.
        /// </summary>
        public LocationEdit()
        {
            this.InitializeComponent();
            this.logic = new Logic();

            if (StockwindowsViewModel.Get.Selected != null)
            {
                this.DataContext = LocationEditViewModel.Get;
            }

            this.KeyDown += this.LocationEdit_KeyDown;
        }

        private void LocationEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bool result = false;
                MessageBoxResult r = MessageBoxResult.No;
                string[] tomb = new string[]
                {
                this.txt_loc_azon.Text,
                this.txt_loc_city.Text,
                this.txt_loc_street.Text,
                this.txt_loc_zipcode.Text,
                this.txt_loc_email.Text,
                };

                try
                {
                    if (this.txt_loc_azon.Text == string.Empty || this.txt_loc_city.Text == string.Empty || this.txt_loc_street.Text == string.Empty || this.txt_loc_zipcode.Text == string.Empty || this.txt_loc_email.Text == string.Empty)
                    {
                        MessageBox.Show("Kérlek töltsd ki a mezőket!", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        if (LocationEditViewModel.Get.Flag == "M")
                        {
                            r = MessageBox.Show("Biztosan módosítani szeretné az adatait ennek a helyszínnek?", "Módosítás", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (r == MessageBoxResult.Yes)
                            {
                                result = this.logic.AddPlace(tomb);
                            }
                        }
                    }

                    if (LocationEditViewModel.Get.Flag == "R")
                    {
                        result = this.logic.AddPlace(tomb);
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres hozzáadta az új helyszínt", string.Empty);
                                });
                            });

                            MessageBox.Show("Sikeres hozzáadta az új helyszínt.", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            this.DialogResult = true;
                        }
                        else if (result == false)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Hiba az új helyszín hozzáadása során", string.Empty);
                                });
                            });

                            MessageBox.Show("Ez a helyszín már szelepel az adatbázisba!", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                    else if (LocationEditViewModel.Get.Flag == "M" && r == MessageBoxResult.Yes)
                    {
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres módosította a helyszínt", string.Empty);
                                });
                            });

                            MessageBox.Show("Sikeres módosította a helyszínt.", "Helyszín módosítotás", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            this.DialogResult = true;
                        }
                    }
                }
                catch (InvalidOperationException ex)
                {
                    Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.logic.LogCreator(ex.Message, string.Empty);
                        });
                    });

                    MessageBox.Show(ex.Message);
                }
            }

            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void Btn_save_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            MessageBoxResult r = MessageBoxResult.No;
            string[] tomb = new string[]
            {
                this.txt_loc_azon.Text,
                this.txt_loc_city.Text,
                this.txt_loc_street.Text,
                this.txt_loc_zipcode.Text,
                this.txt_loc_email.Text,
            };

            try
            {
                if (this.txt_loc_azon.Text == string.Empty || this.txt_loc_city.Text == string.Empty || this.txt_loc_street.Text == string.Empty || this.txt_loc_zipcode.Text == string.Empty || this.txt_loc_email.Text == string.Empty)
                {
                    MessageBox.Show("Kérlek töltsd ki a mezőket!", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (LocationEditViewModel.Get.Flag == "M")
                    {
                        r = MessageBox.Show("Biztosan módosítani szeretné az adatait ennek a helyszínnek?", "Módosítás", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (r == MessageBoxResult.Yes)
                        {
                            result = this.logic.AddPlace(tomb);
                        }
                    }
                }

                if (LocationEditViewModel.Get.Flag == "R")
                {
                    result = this.logic.AddPlace(tomb);
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Sikeres hozzáadta az új helyszínt.", string.Empty);
                            });
                        });

                        MessageBox.Show("Sikeres hozzáadta az új helyszínt.", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        this.DialogResult = true;
                    }
                    else if (result == false)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Hiba az új helyszín hozzáadása során", string.Empty);
                            });
                        });

                        MessageBox.Show("Ez a helyszín már szelepel az adatbázisba!", "Helyszín felvétele", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                else if (LocationEditViewModel.Get.Flag == "M" && r == MessageBoxResult.Yes)
                {
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Sikeres módosította a helyszínt", string.Empty);
                            });
                        });

                        MessageBox.Show("Sikeres módosította a helyszínt.", "Helyszín módosítotás", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        this.DialogResult = true;
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator(ex.Message, string.Empty);
                    });
                });

                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
