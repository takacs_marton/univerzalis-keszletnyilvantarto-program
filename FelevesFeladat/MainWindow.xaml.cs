﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Login logIn;
        private Thread downloadTheDb;
        private ILogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.logIn = new Login();
            this.KeyDown += this.KeyDownMethod;

            this.downloadTheDb = new Thread(() => this.logIn.LoginInto("demo", "demo"));
            this.downloadTheDb.Start();

            this.logic = new Logic();
        }

        private void KeyDownMethod(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Login();
            }
        }

        private void Btn_login_Click(object sender, RoutedEventArgs e)
        {
            this.Login();
        }

        private void Btn_reg_Click(object sender, RoutedEventArgs e)
        {
            UserAccountViewModel.Get.Flag = "OR";
            UserAccount uA = new UserAccount();
            uA.Owner = this;

            uA.ShowDialog();
        }

        private void Login()
        {
            this.btn_login.IsEnabled = false;
            this.btn_login.Content = "Kérem várjon...";
            var tempuser = this.logIn.LoginInto(this.txt_username.Text, this.txt_pw.Password.ToString());

            if (tempuser == null)
            {
                this.btn_login.Background = Brushes.Red;
                MessageBox.Show("Hibás felhasználó vagy jelszó!", "Login", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.btn_login.IsEnabled = true;
                this.btn_login.Content = "Bejelentkezés";

                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator("Hibás Bejelentkezés", this.txt_username.Text);
                    });
                });
            }
            else if (tempuser.Tipus == "Admin")
            {
                this.btn_login.Background = Brushes.Green;
                this.btn_login.Foreground = Brushes.White;
                Stock_windows sW = new Stock_windows(this.txt_username.Text, this.txt_pw.Password.ToString());
                MessageBox.Show("Sikeres bejelentkezés!", "Login", MessageBoxButton.OK, MessageBoxImage.Information);

                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator("Admin bejelentkezés", tempuser.Nev);
                    });
                });

                sW.Show();
                this.Close();
            }
            else if (tempuser.Tipus == "Felhasználó")
            {
                this.btn_login.Background = Brushes.Green;
                this.btn_login.Foreground = Brushes.White;
                UserShopping uS = new UserShopping(this.txt_username.Text, this.txt_pw.Password.ToString());
                MessageBox.Show("Sikeres bejelentkezés!", "Login", MessageBoxButton.OK, MessageBoxImage.Information);

                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator("Felhasználó bejelentkezés", tempuser.Nev);
                    });
                });

                uS.Show();
                this.Close();
            }
            else if (tempuser.Tipus == "Szállító")
            {
                this.btn_login.Background = Brushes.Green;
                this.btn_login.Foreground = Brushes.White;
                TransporterWindow tP = new TransporterWindow(this.txt_username.Text, this.txt_pw.Password.ToString());
                MessageBox.Show("Sikeres bejelentkezés!", "Login", MessageBoxButton.OK, MessageBoxImage.Information);

                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator("Szállító bejelentkezés", tempuser.Nev);
                    });
                });

                tP.Show();
                this.Close();
            }
        }
    }
}
