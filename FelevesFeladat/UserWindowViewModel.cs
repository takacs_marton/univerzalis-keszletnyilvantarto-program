﻿// <copyright file="UserWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataManagement;

    /// <summary>
    /// ViewModel for UserWindow
    /// </summary>
    internal class UserWindowViewModel : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserWindowViewModel"/> class.
        /// </summary>
        public UserWindowViewModel()
        {
        }

        /// <summary>
        /// Gets or sets the selected user
        /// </summary>
        public Felhasznalo SelectedUser { get; set; }

        // public ObservableCollection<Felhasznalo> AllUer;

            /// <summary>
            /// Saves
            /// </summary>
        public void Save()
        {
        }
    }
}
