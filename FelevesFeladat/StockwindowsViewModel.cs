﻿// <copyright file="StockwindowsViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BusinessLogic;
    using DataManagement;

    /// <summary>
    /// ViewModel for Stock_windows
    /// </summary>
    internal class StockwindowsViewModel : Bindable
    {
        private static StockwindowsViewModel vM;

        private ILogic logic = new Logic();

        private StockwindowsViewModel()
        {
            this.UpdateList();
        }

        /// <summary>
        /// Gets the viewmodel
        /// </summary>
        public static StockwindowsViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new StockwindowsViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the selected object
        /// </summary>
        public object Selected { get; set; }

        /// <summary>
        /// Gets or sets the list of products
        /// </summary>
        public List<object> Products { get; set; }

        /// <summary>
        /// Gets or sets the list of users
        /// </summary>
        public List<object> Users { get; set; }

        /// <summary>
        /// Gets or sets the list of places
        /// </summary>
        public List<object> Places { get; set; }

        /// <summary>
        /// Gets or sets the flag
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// Updates the lists
        /// </summary>
        public void UpdateList()
        {
            this.Users = this.logic.GetAllUsers().ToList();
            this.Products = this.logic.GetAllProducts().ToList();
            this.Places = this.logic.GetAllPlaces().ToList();
        }

        // private List<object> products;

        // későbbre
        // public List<object> Products
        // {
        //    get
        //    {
        //        return products;
        //    }
        //    set
        //    {
        //        products = value;
        //        PropertyChanged();
        //    }
        // }

        // public IEnumerable<object> UploadTheUsersList()
        // {
        //    return logic.GetAllUsers();
        // }

        // public IEnumerable<object> UploadTheProductsList()
        // {

        // return logic.GetAllProducts();
        // }

        // public IEnumerable<object> UploadThePlacesList()
        // {
        //    return logic.GetAllPlaces();
        // }

            /// <summary>
            /// Calls DeleteSelected
            /// </summary>
        public void DeleteSelected()
        {
            if (this.Selected != null)
            {
                this.logic.DeleteSelected(this.Selected, this.Flag);
            }
        }
    }
}
