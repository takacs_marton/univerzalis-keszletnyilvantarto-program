﻿// <copyright file="UserAccountViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using DataManagement;

    /// <summary>
    /// ViewModel for UserAccount
    /// </summary>
    internal class UserAccountViewModel : INotifyPropertyChanged
    {
        private static UserAccountViewModel vM;

        private UserAccountViewModel()
        {
        }

        /// <summary>
        /// Eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the viewmodel
        /// </summary>
        public static UserAccountViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new UserAccountViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string SelectedName { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string SelectedEmail { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string SelectedPassword { get; set; }

        /// <summary>
        /// Gets or sets the address
        /// </summary>
        public string SelectedAddress { get; set; }

        /// <summary>
        /// Gets or sets the flag
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// Gets or sets the user type
        /// </summary>
        public string SelectedUserType { get; set; }

        /// <summary>
        /// Sets up the account
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="email">email</param>
        /// <param name="password">PW</param>
        /// <param name="address">address</param>
        /// <param name="usertype">type of user</param>
        /// <param name="flag">flag</param>
        public void AccountDataSetup(string name, string email, string password, string address, string usertype, string flag)
        {
            this.SelectedName = name;
            this.SelectedEmail = email;

            // this.SelectedPassword = password;
            this.Flag = flag;
            this.SelectedUserType = usertype;
            this.SelectedAddress = address;
        }

        /// <summary>
        /// Sets up the account
        /// </summary>
        /// <param name="tomb">Collection of properties</param>
        /// <param name="flag">Flag</param>
        public void AccountDataSetup(string[] tomb, string flag)
        {
            this.SelectedName = tomb[0];
            this.SelectedEmail = tomb[1];

            // this.SelectedPassword = tomb[2];
            this.Flag = flag;
            this.SelectedUserType = tomb[4];
            this.SelectedAddress = tomb[3];
        }

        private void OnPropertyChanged([CallerMemberName] string name = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
