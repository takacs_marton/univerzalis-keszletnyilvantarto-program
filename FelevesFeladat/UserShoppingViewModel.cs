﻿// <copyright file="UserShoppingViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using BusinessLogic;

    /// <summary>
    /// ViewModel for UserShopping
    /// </summary>
    public class UserShoppingViewModel : INotifyPropertyChanged
    {
        private static UserShoppingViewModel vM;
        private ILogic logic = new Logic();
        private string username;
        private object selectedproduct;

        private UserShoppingViewModel()
        {
            // BuyProducts = new List<object>();
            this.BuyProducts = new ObservableCollection<TempProduct>();
            this.Price = 0;
        }

        /// <summary>
        /// Eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the viewmodel
        /// </summary>
        public static UserShoppingViewModel Get
        {
            get
            {
                if (vM == null)
                {
                    vM = new UserShoppingViewModel();
                }

                return vM;
            }
        }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public object User { get; set; }

        /// <summary>
        /// Gets or sets the selected product
        /// </summary>
        public object SelectedBuyProduct { get; set; }

        /// <summary>
        /// Gets or sets the price
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected product
        /// </summary>
        public object SelectedProduct
        {
            get
            {
                return this.selectedproduct;
            }

            set
            {
                this.selectedproduct = value;
                this.OnPropertyChanged();
            }
        }

        // public IEnumerable<object> Products { get; set; }
        // public IEnumerable<object> BuyProducts { get; set; }

        /// <summary>
        /// Gets or sets the list of products from temporary class
        /// </summary>
        public List<TempProduct> Products { get; set; }

        // public List<object> BuyProducts { get; set; }

            /// <summary>
            /// Gets or sets the collection of products from temporary class
            /// </summary>
        public ObservableCollection<TempProduct> BuyProducts { get; set; }

        // public void MoveToBuy(string[] selected, int quantity, object obj)
        // {
        //    TempProduct tmo = UserShoppingViewModel.Get.Products.Where(e => e.Termekazon == decimal.Parse(selected[0])).FirstOrDefault();
        //    tmo.Darabszam = tmo.Darabszam - quantity;
        //    this.BuyProducts.Add(obj);

        // this.PriceUpdate(true);
        //    this.OnPropertyChanged("Price");
        // }

            /// <summary>
            /// Moves the bought item to other window
            /// </summary>
            /// <param name="selected">Selected item</param>
            /// <param name="quantity">How much</param>
            /// <param name="obj">Parameter container</param>
        public void MoveToBuy(string[] selected, int quantity, TempProduct obj) // próba
        {
            TempProduct tmo = UserShoppingViewModel.Get.Products.Where(e => e.Termekazon == decimal.Parse(selected[0])).FirstOrDefault();
            tmo.Darabszam = tmo.Darabszam - quantity;

            TempProduct shopped;
            try
            {
                shopped = this.BuyProducts.Where(p => p.Termekazon == obj.Termekazon).First();
            }
            catch (Exception)
            {
                shopped = null;
            }

            if (shopped == null)
            {
              shopped = new TempProduct(obj.Termekazon, obj.Nev, obj.Megjegyzes, obj.Ar, obj.Helyszinazon, quantity);
              this.BuyProducts.Add(shopped);
            }
            else
            {
                shopped.Darabszam += quantity;
            }

            this.PriceUpdate(true);
            this.OnPropertyChanged("Price");
        }

        /// <summary>
        /// Removes from bought products
        /// </summary>
        /// <param name="selected">Selected item</param>
        public void RemoveFromBuyProducts(TempProduct selected)
        {
            try
            {
                this.BuyProducts.Remove(selected);
                this.PriceUpdate(false);
                this.OnPropertyChanged("Price");
                TempProduct tmo = UserShoppingViewModel.Get.Products.Where(e => e.Termekazon == selected.Termekazon).FirstOrDefault();
                tmo.Darabszam = tmo.Darabszam + selected.Darabszam;
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Gets specific products
        /// </summary>
        /// <param name="amit">Name of product</param>
        /// <returns>List of matching products</returns>
        public List<TempProduct> GetSpecificProducts(string amit)
        {
            return UserShoppingViewModel.Get.Products.Where(e => e.Nev.Contains(amit)).ToList();
        }

        /// <summary>
        /// Updates price of item
        /// </summary>
        /// <param name="buy">Whether we bought it</param>
        public void PriceUpdate(bool buy)
        {
            this.Price = 0;
            object p = 0;
            foreach (var selectedp in this.BuyProducts)
            {
                Type t = selectedp.GetType();
                PropertyInfo[] props = t.GetProperties();
                foreach (PropertyInfo att in props)
                {
                    if (att.Name == "Ar")
                    {
                        p = att.GetValue(selectedp, null);
                        if (buy)
                        {
                            this.Price += Convert.ToInt32(p) * Convert.ToInt32(props[5].GetValue(selectedp));
                        }
                        else
                        {
                            this.Price -= Convert.ToInt32(p) * Convert.ToInt32(props[5].GetValue(selectedp)) * -1;
                        }
                    }
                }
            }

            this.OnPropertyChanged("Price");
        }

        private void OnPropertyChanged([CallerMemberName] string name = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
