﻿// <copyright file="QuantitySet.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for QuantitySet.xaml
    /// </summary>
    public partial class QuantitySet : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QuantitySet"/> class.
        /// </summary>
        public QuantitySet()
        {
            this.InitializeComponent();
            this.DataContext = QuantitySetViewModel.Get;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (int.Parse(this.textBox.Text) > 0)
            {
                QuantitySetViewModel.Get.SelectedQuantity = int.Parse(this.textBox.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("Nem adhat meg 1-nél kisebb értéket!", "Figyelmeztetés", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }
    }
}
