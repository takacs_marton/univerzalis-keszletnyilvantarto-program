﻿// <copyright file="UserAccount.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using BusinessLogic;

    /// <summary>
    /// UserAccount window
    /// </summary>
    public partial class UserAccount : Window
    {
        private ILogic logic;

        // private UserWindowViewModel UWVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccount"/> class.
        /// </summary>
        public UserAccount()
        {
            this.InitializeComponent();
            this.logic = new Logic();

            if (StockwindowsViewModel.Get.Selected != null || UserShoppingViewModel.Get.User != null || TransporterWindowViewModel.Get.User != null)
            {
                this.DataContext = UserAccountViewModel.Get;

                if (UserAccountViewModel.Get.SelectedUserType == "Admin")
                {
                    this.txt_email.IsEnabled = false;
                }
            }

            if (UserAccountViewModel.Get.Flag == "UM" || UserAccountViewModel.Get.Flag == "TM")
            {
                this.txt_email.IsEnabled = false;
            }

            if (UserAccountViewModel.Get.Flag == "OR" || UserAccountViewModel.Get.Flag == "UM" || UserAccountViewModel.Get.Flag == "TM")
            {
                this.comboBox_type.IsEnabled = false;
            }

            this.KeyDown += this.KeyDownMethod;
        }

        private void KeyDownMethod(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bool result = false;
                var hash = SecurePasswordHasher.Hash(this.txt_pw.Password);
                string[] tomb = new string[]
                {
                this.txt_username.Text,
                hash.ToString(),
                this.txt_email.Text,
                this.txt_address.Text,
                this.comboBox_type.Text
                };

                if (UserAccountViewModel.Get.Flag == "OR")
                {
                    tomb[4] = "Felhasználó";
                }

                try
                {
                    if (this.txt_username.Text == string.Empty || this.txt_pw.Password == string.Empty || this.txt_email.Text == string.Empty || this.txt_address.Text == string.Empty)
                    {
                        MessageBox.Show("Kérlek töltsd ki a mezőket!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else if (!this.txt_email.Text.Contains("@"))
                    {
                        MessageBox.Show("Az e-mail címnek tartalmaznia kell @ jelet. Ellenőrizd a megadott adatot.", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        result = this.logic.AddUser(tomb);
                    }

                    if (UserAccountViewModel.Get.Flag == "R" || UserAccountViewModel.Get.Flag == "OR")
                    {
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres Regisztráció", this.txt_username.Text);
                                });
                            });

                            MessageBox.Show("Sikeres Regisztráció.", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            this.DialogResult = true;
                        }
                        else if (result == false)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Hiba a regisztráció során", this.txt_username.Text);
                                });
                            });

                            MessageBox.Show("A megadott felhasználó név már foglalt!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                    else if (UserAccountViewModel.Get.Flag == "M" || UserAccountViewModel.Get.Flag == "UM" || UserAccountViewModel.Get.Flag == "TM")
                    {
                        if (result == true)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Sikeres módosítás", this.txt_username.Text);
                                });
                            });

                            MessageBox.Show("Sikeres módosítás.", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Information);
                            StockwindowsViewModel.Get.UpdateList();
                            UserShoppingViewModel.Get.Username = tomb[0];
                            this.DialogResult = true;
                        }
                        else if (result == false)
                        {
                            Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    this.logic.LogCreator("Hiba a módosítás során", this.txt_username.Text);
                                });
                            });

                            MessageBox.Show("Hiba a módosítás során!", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                catch (InvalidOperationException ex)
                {
                    Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.logic.LogCreator(ex.Message, this.txt_username.Text);
                        });
                    });

                    MessageBox.Show(ex.Message);
                }
            }

            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void Btn_save_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            var hash = SecurePasswordHasher.Hash(this.txt_pw.Password);
            string[] tomb = new string[]
            {
                this.txt_username.Text,
                hash.ToString(),
                this.txt_email.Text,
                this.txt_address.Text,
                this.comboBox_type.Text
            };

            if (UserAccountViewModel.Get.Flag == "OR")
            {
                tomb[4] = "Felhasználó";
            }

            try
            {
                if (this.txt_username.Text == string.Empty || this.txt_pw.Password == string.Empty || this.txt_email.Text == string.Empty || this.txt_address.Text == string.Empty)
                {
                    MessageBox.Show("Kérlek töltsd ki a mezőket!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else if (!this.txt_email.Text.Contains("@"))
                {
                    MessageBox.Show("Az e-mail címnek tartalmaznia kell @ jelet. Ellenőrizd a megadott adatot.", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    result = this.logic.AddUser(tomb);
                }

                if (UserAccountViewModel.Get.Flag == "R" || UserAccountViewModel.Get.Flag == "OR")
                {
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Új regisztráció", this.txt_username.Text);
                            });
                        });

                        MessageBox.Show("Sikeres Regisztráció.", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        this.DialogResult = true;
                    }
                    else if (result == false)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Hiba a regisztráció során", this.txt_username.Text);
                            });
                        });

                        MessageBox.Show("A megadott felhasználó név már foglalt!", "Regisztráció", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                else if (UserAccountViewModel.Get.Flag == "M" || UserAccountViewModel.Get.Flag == "UM" || UserAccountViewModel.Get.Flag == "TM")
                {
                    if (result == true)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Sikeres módosítás", this.txt_username.Text);
                            });
                        });

                        MessageBox.Show("Sikeres módosítás.", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Information);
                        StockwindowsViewModel.Get.UpdateList();
                        UserShoppingViewModel.Get.Username = tomb[0];
                        this.DialogResult = true;
                    }
                    else if (result == false)
                    {
                        Task.Run(() =>
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                this.logic.LogCreator("Hiba a módosítás során", this.txt_username.Text);
                            });
                        });

                        MessageBox.Show("Hiba a módosítás során!", "Módosítás", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                Task.Run(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.logic.LogCreator(ex.Message, this.txt_username.Text);
                    });
                });

                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}