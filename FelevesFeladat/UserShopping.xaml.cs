﻿// <copyright file="UserShopping.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for UserShopping.xaml
    /// </summary>
    public partial class UserShopping : Window
    {
        private ILogic logic;
        private Login logIn;
        private List<TempProduct> save;
        private bool logout = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserShopping"/> class.
        /// </summary>
        /// <param name="username">Username of logged in</param>
        /// <param name="password">PW of logged in</param>
        public UserShopping(string username, string password)
        {
            this.InitializeComponent();
            this.logIn = new Login();
            this.logic = new Logic();
            var tempuser = this.logIn.LoginInto(username, password);

            UserShoppingViewModel.Get.Products = this.logic.GetTempProductsList();
            UserShoppingViewModel.Get.User = tempuser;
            UserShoppingViewModel.Get.Username = "Üdvözöllek: " + tempuser.Nev;
            this.DataContext = UserShoppingViewModel.Get;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.logout)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void Btn_cart_Click(object sender, RoutedEventArgs e)
        {
            string[] tmp = this.logic.Find(UserShoppingViewModel.Get.SelectedProduct, "P");

            QuantitySet qs = new QuantitySet();
            qs.ShowDialog();

            if (QuantitySetViewModel.Get.SelectedQuantity > int.Parse(tmp[5]))
            {
                MessageBox.Show("Túl nagy a kért darabszám!");
            }
            else
            {
                UserShoppingViewModel.Get.MoveToBuy(tmp, QuantitySetViewModel.Get.SelectedQuantity, UserShoppingViewModel.Get.SelectedProduct as TempProduct);
            }

            this.Datagrid_Products.Items.Refresh();
            this.Datagrid_BuyProducts.Items.Refresh();
        }

        private void Btn_cart_remove_Click(object sender, RoutedEventArgs e)
        {
            string[] tmp = this.logic.Find(UserShoppingViewModel.Get.SelectedBuyProduct, "P");
            UserShoppingViewModel.Get.RemoveFromBuyProducts(UserShoppingViewModel.Get.SelectedBuyProduct as TempProduct);
            this.Datagrid_Products.Items.Refresh();
        }

        private void Btn_search_Click(object sender, RoutedEventArgs e)
        {
            if (this.textBox_search.Text == string.Empty)
            {
                foreach (var mentett in this.save.ToArray())
                {
                    foreach (var modositott in UserShoppingViewModel.Get.Products.ToArray())
                    {
                        if (mentett.Termekazon == modositott.Termekazon && mentett.Darabszam != modositott.Darabszam)
                        {
                            this.save.Remove(mentett);
                            this.save.Add(modositott);
                        }
                    }
                }

                this.Datagrid_Products.ItemsSource = this.save;
                this.lbl_stat.Content = this.save.Count();
            }
            else
            {
                if (UserShoppingViewModel.Get.Products.Count == 5)
                {
                    this.save = UserShoppingViewModel.Get.Products;
                }

                foreach (var mentett in this.save.ToArray())
                {
                    foreach (var modositott in UserShoppingViewModel.Get.Products.ToArray())
                    {
                        if (mentett.Termekazon == modositott.Termekazon && mentett.Darabszam != modositott.Darabszam)
                        {
                            this.save.Remove(mentett);
                            this.save.Add(modositott);
                        }
                    }
                }

                // UserShoppingViewModel.Get.Products = this.logic.GetSpecificProducts(this.textBox_search.Text).ToList();
                UserShoppingViewModel.Get.Products = this.logic.GetSpecificProducts(this.textBox_search.Text);
                this.Datagrid_Products.ItemsSource = UserShoppingViewModel.Get.Products;
                this.lbl_stat.Content = UserShoppingViewModel.Get.Products.Count();
            }
        }

        private void Btn_useraccountmodify_Click(object sender, RoutedEventArgs e)
        {
            string[] tmp = this.logic.Find(UserShoppingViewModel.Get.User, "U");

            UserAccountViewModel.Get.AccountDataSetup(tmp, "UM");
            UserAccount ua = new UserAccount();
            ua.ShowDialog();
        }

        private void Btn_shop_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Befelyezi a vásárlást?", "Vásárlás", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                this.logic.SaveBoughtProducts(UserShoppingViewModel.Get.BuyProducts);
                MessageBox.Show("Köszönjük a vásárlást. \nA Végösszeg: " + UserShoppingViewModel.Get.Price, "Vásárlás befelyezése");
                UserShoppingViewModel.Get.BuyProducts.Clear();

                // UserShoppingViewModel.Get.Price = 0;
                UserShoppingViewModel.Get.PriceUpdate(true);
            }
            else
            {
                return;
            }
        }

        private void Btn_logout_Click(object sender, RoutedEventArgs e)
        {
            this.logout = true;
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }
    }
}
