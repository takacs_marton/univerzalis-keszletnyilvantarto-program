﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataManagement;

    /// <summary>
    /// implementation of ILogic and a bit more
    /// </summary>
    public class Logic : ILogic
    {
        private IDbRepo dataRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="dbrepo"> the needed IDbRepo</param>
        public Logic(IDbRepo dbrepo)
        {
            this.dataRepo = dbrepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Without IDbRepo
        /// </summary>
        public Logic()
        {
            this.dataRepo = new DbRepo();
            this.DefaultAdmin();
            this.DefaultUser();
            this.DefaultTrans();
        }

        /// <summary>
        /// Adds new user
        /// </summary>
        /// <param name="newUser">new user parameters</param>
        /// <returns>success</returns>
        public bool AddUser(string[] newUser)
        {
                Felhasznalo nu = new Felhasznalo()
                {
                    Nev = newUser[0],
                    Jelszo = newUser[1],
                    Email = newUser[2],
                    Lakcim = newUser[3],
                    Tipus = newUser[4],
                    Kosarazon = 0,
                    Vegosszeg = 0
                };

                this.dataRepo.AddUser(nu);
                return true;
        }

        /// <summary>
        /// Adds new product
        /// </summary>
        /// <param name="newProduct">the new product's parameters</param>
        /// <returns>success</returns>
        public bool AddProduct(string[] newProduct)
        {
                Termek np = new Termek()
                {
                    Termekazon = int.Parse(newProduct[0]),
                    Nev = newProduct[1],
                    Megjegyzes = newProduct[2],
                    Ar = int.Parse(newProduct[3]),
                    Helyszinazon = int.Parse(newProduct[4]),
                    Darabszam = int.Parse(newProduct[5])
                };

                this.dataRepo.AddProduct(np);
                return true;
        }

        /// <summary>
        /// Adds new place
        /// </summary>
        /// <param name="newPlace">new place parameters</param>
        /// <returns>success</returns>
        public bool AddPlace(string[] newPlace)
        {
            // var tempquery = this.dataRepo.GetAllPlaces().Where(e => e.Helyszinazon == int.Parse(newPlace[0])).Count();
            var user = this.dataRepo.GetAllUsers().Where(e => e.Email == newPlace[4]).FirstOrDefault();

            // if (tempquery == 0)
            // {
            //    Helyszin np = new Helyszin()
            //    {
            //        Helyszinazon = int.Parse(newPlace[0]),
            //        Varos = newPlace[1],
            //        Utca = newPlace[2],
            //        Iranyitoszam = int.Parse(newPlace[3]),
            //        Email = newPlace[4],
            //        Felhasznalo = user
            //    };

            // this.dataRepo.AddPlace(np);
            //    return true;
            // }

            // return false;
            Helyszin np = new Helyszin()
            {
                Helyszinazon = int.Parse(newPlace[0]),
                Varos = newPlace[1],
                Utca = newPlace[2],
                Iranyitoszam = int.Parse(newPlace[3]),
                Email = newPlace[4],
                Felhasznalo = user
            };

            this.dataRepo.AddPlace(np);
            return true;
        }

        /// <summary>
        /// gets all users
        /// </summary>
        /// <returns>collection of users</returns>
        public IEnumerable<object> GetAllUsers()
        {
            return this.dataRepo.GetAllUsers().Select(e => new
            {
                Név = e.Nev,
                Típus = e.Tipus,
                Email = e.Email,
                Lakcím = e.Lakcim
            });
        }

        /// <summary>
        /// gets all places
        /// </summary>
        /// <returns>collection of places</returns>
        public IEnumerable<object> GetAllPlaces()
        {
            return this.dataRepo.GetAllPlaces().Select(e => new
            {
                Helyszinazon = e.Helyszinazon,
                Irányítószám = e.Iranyitoszam,
                Város = e.Varos,
                Utca = e.Utca,
                Felelős = e.Felhasznalo.Email
            });
        }

        /// <summary>
        /// gets all products
        /// </summary>
        /// <returns>collection of products</returns>
        public IEnumerable<object> GetAllProducts()
        {
            return this.dataRepo.GetAllProducts().Select(e => new
            {
                Termekazon = e.Termekazon,
                Név = e.Nev,
                Ár = e.Ar,
                Megjegyzés = e.Megjegyzes,
                Darabszám = e.Darabszam
            });
        }

        /// <summary>
        /// gets all logs
        /// </summary>
        /// <returns>collection of logs</returns>
        public IEnumerable<object> GetAllLog()
        {
            return this.dataRepo.GetAllLog().Select(e => new
            {
                Azonosito = e.Azonosito,
                Üzenet = e.Uzenet,
                Felhasználó = e.Felhasznalo,
                Dátum = e.Datum
            });
        }

        /// <summary>
        /// gets specific products
        /// </summary>
        /// <param name="amit">the name of product</param>
        /// <returns>list of products</returns>
        public List<TempProduct> GetSpecificProducts(string amit)
        {
            // return this.GetTempProductsList().Where(k => k.Nev.Contains(amit) || k.Megjegyzes.Contains(amit)).Select(e => new
            // {
            //    Név = e.Nev,
            //    Ár = e.Ar,
            //    Termekazon = e.Termekazon,
            //    Darabszám = e.Darabszam,
            //    Megjegyzés = e.Megjegyzes
            // });
            return this.GetTempProductsList().Where(e => e.Nev.ToLower().Contains(amit.ToLower())).ToList();
        }

        /// <summary>
        /// calls DeleteSelected
        /// </summary>
        /// <param name="selected">selected item</param>
        /// <param name="flag">either U,P,or L</param>
        public void DeleteSelected(object selected, string flag)
        {
            this.dataRepo.DeleteSelected(selected, flag);
        }

        /// <summary>
        /// calls Find
        /// </summary>
        /// <param name="selected">selected item</param>
        /// <param name="flag">either U, P or L</param>
        /// <returns>found item parameters</returns>
        public string[] Find(object selected, string flag)
        {
            return this.dataRepo.Find(selected, flag);
        }

        /// <summary>
        /// makes new Termek with set darabszam
        /// </summary>
        /// <param name="newProduct">the new product's parameters</param>
        /// <param name="db">darabszam number</param>
        /// <returns>new Termek</returns>
        public Termek SetTheBuyQuantity(string[] newProduct, int db)
        {
            // Helyszin tmphelszin = this.dataRepo.GetAllProducts().Where(e => int.Parse(newProduct[0]) == e.Termekazon).FirstOrDefault();
            Termek np = new Termek();
            np.Termekazon = int.Parse(newProduct[0]);
            np.Nev = newProduct[1];
            np.Megjegyzes = newProduct[4];
            np.Ar = int.Parse(newProduct[2]);
            np.Helyszinazon = int.Parse(newProduct[3]);

            // np.Helyszin = tmphelszin;
            np.Darabszam = Convert.ToInt32(db);

            // Termek tosave = new Termek()
            // {
            //    Termekazon = int.Parse(newProduct[0]),
            //    Nev = newProduct[1],
            //    Megjegyzes = newProduct[4],
            //    Ar = int.Parse(newProduct[2]),
            //    Helyszinazon = int.Parse(newProduct[3]),
            //    Darabszam = int.Parse(newProduct[5]) - db
            //    //Helyszin = tmphelszin
            // };

            // this.dataRepo.AddProduct(tosave);
            return np;
        }

        /// <summary>
        /// calls DecreaseQuantity
        /// </summary>
        /// <param name="azon">termekazon</param>
        /// <param name="quantity">darabszam number</param>
        public void DecreaseQuantity(int azon, int quantity)
        {
            this.dataRepo.DecreaseQuantity(azon, quantity);
        }

        // public object GetSpecObj(int Termekazon)
        // {
        //    return this.dataRepo.GetAllProducts().Where(e => Termekazon == e.Termekazon).FirstOrDefault();
        // }

        /// <summary>
        /// gets a specific product
        /// </summary>
        /// <param name="termekazon">termekazon</param>
        /// <returns>parameters of product</returns>
        public object GetSpecObj(int termekazon)
        {
            return this.dataRepo.GetAllProducts().Where(e => termekazon == e.Termekazon).Select(e => new
            {
                Termekazon = e.Termekazon,
                Név = e.Nev,
                Ár = e.Ar,
                Megjegyzés = e.Megjegyzes,
                Darabszám = e.Darabszam
            }).FirstOrDefault();
        }

        /// <summary>
        /// gets a list of products for display
        /// </summary>
        /// <returns>list</returns>
        public List<TempProduct> GetTempProductsList()
        {
            List<TempProduct> lista = new List<TempProduct>();
            foreach (var item in this.GetAllProducts().ToList())
            {
                string[] temp = this.dataRepo.Find(item, "P");
                TempProduct newproduct = new TempProduct(decimal.Parse(temp[0]), temp[1], temp[4], int.Parse(temp[2]), int.Parse(temp[3]), int.Parse(temp[5]));
                lista.Add(newproduct);
            }

            return lista;
        }

        /// <summary>
        /// calls addproduct
        /// </summary>
        /// <param name="product">the new product</param>
        public void AddProduct(Termek product)
        {
            this.dataRepo.AddProduct(product);
        }

        /// <summary>
        /// saves the bought products
        /// </summary>
        /// <param name="buyProducts">list of products</param>
        public void SaveBoughtProducts(ObservableCollection<TempProduct> buyProducts)
        {
            foreach (var item in buyProducts)
            {
                Termek temptermek = this.dataRepo.GetAllProducts().Where(e => e.Termekazon == item.Termekazon).FirstOrDefault();

                Termek np = new Termek()
                {
                    Termekazon = item.Termekazon,
                    Nev = item.Nev,
                    Megjegyzes = item.Megjegyzes,
                    Ar = item.Ar,
                    Helyszinazon = item.Helyszinazon,
                    Darabszam = temptermek.Darabszam - item.Darabszam
                };

                this.AddProduct(np);
            }
        }

        // public void LogCreator(string message, Felhasznalo creator)
        // {
        //    int db = this.dataRepo.GetAllLog().ToList().Count();

        // Log newlog = new Log()
        //    {
        //        Azonosito = db++,
        //        Uzenet = message,
        //        Datum = DateTime.Now.Date,
        //        Felhasznalo = creator.Nev
        //    };

        // this.dataRepo.AddLog(newlog);
        // }

            /// <summary>
            /// creates a log
            /// </summary>
            /// <param name="message">the contains of the message</param>
            /// <param name="username">who sent it</param>
        public void LogCreator(string message, string username)
        {
            int db = this.dataRepo.GetAllLog().ToList().Count();
            Log newlog = new Log()
            {
                Azonosito = db++,
                Uzenet = message,
                Datum = DateTime.Now + DateTime.Today.TimeOfDay,
                Felhasznalo = username
            };

            this.dataRepo.AddLog(newlog);
        }

        private void DefaultAdmin()
        {
                Felhasznalo newadmin = new Felhasznalo()
                {
                    Nev = "Admin",
                    Tipus = "Admin",
                    Jelszo = SecurePasswordHasher.Hash("Admin"),
                    Email = "Admin",
                    Lakcim = "Admin"
                };

                this.dataRepo.AddUser(newadmin);
        }

        private void DefaultUser()
        {
            Felhasznalo newuser = new Felhasznalo()
            {
                Nev = "User",
                Tipus = "Felhasználó",
                Jelszo = SecurePasswordHasher.Hash("User"),
                Email = "User",
                Lakcim = "User"
            };

            this.dataRepo.AddUser(newuser);
        }

        private void DefaultTrans()
        {
            Felhasznalo newtrans = new Felhasznalo()
            {
                Nev = "Trans",
                Tipus = "Szállító",
                Jelszo = SecurePasswordHasher.Hash("Trans"),
                Email = "Trans",
                Lakcim = "Trans"
            };

            this.dataRepo.AddUser(newtrans);
        }
    }
}
