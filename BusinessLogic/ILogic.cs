﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataManagement;

    /// <summary>
    /// ILogic for the GUI
    /// </summary>
    public interface ILogic
    {
        // List<Felhasznalo> GetAllUsers();
        // List<Termek> GetAllProducts();

        /// <summary>
        /// gets all users from Db
        /// </summary>
        /// <returns>Users</returns>
        IEnumerable<object> GetAllUsers();

        /// <summary>
        /// gets all products from Db
        /// </summary>
        /// <returns>products</returns>
        IEnumerable<object> GetAllPlaces();

        /// <summary>
        /// gets all places from db
        /// </summary>
        /// <returns>places</returns>
        IEnumerable<object> GetAllProducts();

        /// <summary>
        /// gets all logs
        /// </summary>
        /// <returns>logs</returns>
        IEnumerable<object> GetAllLog();

        /// <summary>
        /// gets specific products
        /// </summary>
        /// <param name="amit">the name of the product</param>
        /// <returns>List of specific products</returns>
        List<TempProduct> GetSpecificProducts(string amit);

        /// <summary>
        /// adds new user
        /// </summary>
        /// <param name="newUser">new user</param>
        /// <returns>success</returns>
        bool AddUser(string[] newUser);

        /// <summary>
        /// adds new product
        /// </summary>
        /// <param name="newProduct">new product</param>
        /// <returns>success</returns>
        bool AddProduct(string[] newProduct);

        /// <summary>
        /// adds new place
        /// </summary>
        /// <param name="newPlace">new place</param>
        /// <returns>success</returns>
        bool AddPlace(string[] newPlace);

        /// <summary>
        /// deletes selected whatever
        /// </summary>
        /// <param name="selected">the selected object</param>
        /// <param name="flag">What it is</param>
        void DeleteSelected(object selected, string flag);

        /// <summary>
        /// finds the selected
        /// </summary>
        /// <param name="selected">selected object</param>
        /// <param name="flag">what it is</param>
        /// <returns>item properties</returns>
        string[] Find(object selected, string flag);

        /// <summary>
        /// Sets the buy quantity
        /// </summary>
        /// <param name="product">the product</param>
        /// <param name="db">how much</param>
        /// <returns>new Termek</returns>
        Termek SetTheBuyQuantity(string[] product, int db);

        /// <summary>
        /// decreases the quantity of item
        /// </summary>
        /// <param name="azon">termekazon of item</param>
        /// <param name="quantity">how much</param>
        void DecreaseQuantity(int azon, int quantity);

        /// <summary>
        /// gets specific object
        /// </summary>
        /// <param name="termekazon">termekazon of item</param>
        /// <returns>the object</returns>
        object GetSpecObj(int termekazon);

        /// <summary>
        /// gets product list for displaying from Db
        /// </summary>
        /// <returns>list of products</returns>
        List<TempProduct> GetTempProductsList();

        /// <summary>
        /// saves the bought products
        /// </summary>
        /// <param name="buyProducts">bought products</param>
        void SaveBoughtProducts(ObservableCollection<TempProduct> buyProducts);

        // void PasswordConverter();
        // void LogCreator(string message, Felhasznalo creator);

        /// <summary>
        /// creates log of happenings
        /// </summary>
        /// <param name="message">what the message should be about</param>
        /// <param name="username">who did it</param>
        void LogCreator(string message, string username);
    }
}
