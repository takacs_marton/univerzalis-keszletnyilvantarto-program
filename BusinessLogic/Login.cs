﻿// <copyright file="Login.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataManagement;

    /// <summary>
    /// Login
    /// </summary>
    public class Login
    {
        private object lockLogin = new object();
        private DB adatbazis;

        /// <summary>
        /// Initializes a new instance of the <see cref="Login"/> class.
        /// </summary>
        public Login()
        {
            this.adatbazis = new DB();
        }

        /// <summary>
        /// Login method
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">user password</param>
        /// <returns>User from database</returns>
        public Felhasznalo LoginInto(string email, string password)
        {
            try
            {
                // var result = this.adatbazis.GetAllUsers().Where(e => e.Email == email && e.Jelszo == password).First();
                lock (this.lockLogin)
                {
                    var result = this.adatbazis.GetAllUsers().Where(e => e.Email == email).First();
                    var hashresult = SecurePasswordHasher.Verify(password, result.Jelszo);
                    if (hashresult == true)
                    {
                        return result;
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// returns database
        /// </summary>
        /// <returns>database</returns>
        public DB GetDb()
        {
            return this.adatbazis;
        }
    }
}
