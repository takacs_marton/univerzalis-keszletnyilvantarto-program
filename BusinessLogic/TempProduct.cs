﻿// <copyright file="TempProduct.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// TempProduct class for storing products
    /// </summary>
    public class TempProduct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TempProduct"/> class.
        /// </summary>
        /// <param name="termekazon">termekazon</param>
        /// <param name="nev">nev</param>
        /// <param name="megjegyzes">megjegyzes</param>
        /// <param name="ar">ar</param>
        /// <param name="helyszinazon">helyszinazon</param>
        /// <param name="darabszam">darabszam</param>
        public TempProduct(decimal termekazon, string nev, string megjegyzes, int ar, int helyszinazon, int darabszam)
        {
            this.Termekazon = termekazon;
            this.Nev = nev;
            this.Megjegyzes = megjegyzes;
            this.Ar = ar;
            this.Helyszinazon = helyszinazon;
            this.Darabszam = darabszam;
        }

        /// <summary>
        /// Gets or sets termekazon
        /// </summary>
        public decimal Termekazon { get; set; }

        /// <summary>
        /// Gets or sets nev
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets megjegyzes
        /// </summary>
        public string Megjegyzes { get; set; }

        /// <summary>
        /// Gets or sets ar
        /// </summary>
        public int Ar { get; set; }

        /// <summary>
        /// Gets or sets helyszinazon
        /// </summary>
        public int Helyszinazon { get; set; }

        /// <summary>
        /// Gets or sets darabszam
        /// </summary>
        public int Darabszam { get; set; }
    }
}
