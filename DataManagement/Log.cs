//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataManagement
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// Log class
    /// </summary>
    public partial class Log
    {
        /// <summary>
        /// Gets or sets azonosito
        /// </summary>
        public decimal Azonosito { get; set; }

        /// <summary>
        /// Gets or sets felhasznalo
        /// </summary>
        public string Felhasznalo { get; set; }

        /// <summary>
        /// Gets or sets uzenet
        /// </summary>
        public string Uzenet { get; set; }

        /// <summary>
        /// Gets or sets date
        /// </summary>
        public System.DateTime Datum { get; set; }
    }
}
