﻿// <copyright file="IDbRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// interface for using Db
    /// </summary>
    public interface IDbRepo
    {
        // IQueryable<Felhasznalo> GetAllUSer();
        // IQueryable<Termek> GetAllProduct();

            /// <summary>
            /// gets all users
            /// </summary>
            /// <returns>collection of users</returns>
        IEnumerable<Felhasznalo> GetAllUsers();

        /// <summary>
        /// gets all products
        /// </summary>
        /// <returns>collection of products</returns>
        IEnumerable<Termek> GetAllProducts();

        /// <summary>
        /// gets all places
        /// </summary>
        /// <returns>collection of places</returns>
        IEnumerable<Helyszin> GetAllPlaces();

        /// <summary>
        /// gets all logs
        /// </summary>
        /// <returns>collection of logs</returns>
        IEnumerable<Log> GetAllLog();

        /// <summary>
        /// adds new user
        /// </summary>
        /// <param name="newUser">the new user</param>
        void AddUser(Felhasznalo newUser);

        /// <summary>
        /// adds new product
        /// </summary>
        /// <param name="newProduct">the new product</param>
        void AddProduct(Termek newProduct);

        /// <summary>
        /// adds new place
        /// </summary>
        /// <param name="newPlace">the new place</param>
        void AddPlace(Helyszin newPlace);

        /// <summary>
        /// adds new log
        /// </summary>
        /// <param name="newlog">the new log</param>
        void AddLog(Log newlog);

        /// <summary>
        /// deletes selected item
        /// </summary>
        /// <param name="selected">the selected item</param>
        /// <param name="flag">flag that determines it's table</param>
        void DeleteSelected(object selected, string flag);

        /// <summary>
        /// Finds selected item
        /// </summary>
        /// <param name="selected">Selected item</param>
        /// <param name="flag">Flag that determines it's table</param>
        /// <returns>Item properties</returns>
        string[] Find(object selected, string flag);

        /// <summary>
        /// Decreases the quantity of products
        /// </summary>
        /// <param name="azon">termekazon</param>
        /// <param name="quantity">quantity</param>
        void DecreaseQuantity(int azon, int quantity);
    }
}
