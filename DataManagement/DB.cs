﻿// <copyright file="DB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataManagement
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Database initialiser
    /// </summary>
    public class DB
    {
        private dbEntities entities = new dbEntities();

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>Collection of users</returns>
        public IEnumerable<Felhasznalo> GetAllUsers()
        {
            return this.entities.Felhasznalo.AsEnumerable<Felhasznalo>();
        }
    }
}
