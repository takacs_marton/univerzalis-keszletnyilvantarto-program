﻿// <copyright file="DbRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataManagement
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implements IDbRepo
    /// </summary>
    public class DbRepo : IDbRepo
    {
        private dbEntities dBE;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbRepo"/> class.
        /// </summary>
        public DbRepo()
        {
            this.dBE = new dbEntities();
        }

        /// <summary>
        /// Adds new product to Db
        /// </summary>
        /// <param name="newProduct">The new product</param>
        public void AddProduct(Termek newProduct)
        {
                this.dBE.Termek.AddOrUpdate(newProduct);
                this.dBE.SaveChanges();
        }

        /// <summary>
        /// Adds new place to Db
        /// </summary>
        /// <param name="newPlace">New place</param>
        public void AddPlace(Helyszin newPlace)
        {
                this.dBE.Helyszin.AddOrUpdate(newPlace);
                this.dBE.SaveChanges();
        }

        /// <summary>
        /// Adds new user to Db
        /// </summary>
        /// <param name="newUser">New user</param>
        public void AddUser(Felhasznalo newUser)
        {
                this.dBE.Felhasznalo.AddOrUpdate(newUser);
                this.dBE.SaveChanges();
        }

        /// <summary>
        /// Adds new log
        /// </summary>
        /// <param name="newlog">New log</param>
        public void AddLog(Log newlog)
        {
            this.dBE.Log.AddOrUpdate(newlog);
            this.dBE.SaveChanges();
        }

        /// <summary>
        /// Gets all places
        /// </summary>
        /// <returns>Collection of places</returns>
        public IEnumerable<Helyszin> GetAllPlaces()
        {
                return this.dBE.Helyszin.AsEnumerable<Helyszin>();
        }

        /// <summary>
        /// Gets all products
        /// </summary>
        /// <returns>Collection of products</returns>
        public IEnumerable<Termek> GetAllProducts()
        {
                return this.dBE.Termek.AsEnumerable<Termek>();
        }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns>Collection of users</returns>
        public IEnumerable<Felhasznalo> GetAllUsers()
        {
                return this.dBE.Felhasznalo.AsEnumerable<Felhasznalo>();
        }

        /// <summary>
        /// Gets all logs
        /// </summary>
        /// <returns>Collection of logs</returns>
        public IEnumerable<Log> GetAllLog()
        {
            return this.dBE.Log.AsEnumerable<Log>();
        }

        /// <summary>
        /// Deletes selected item
        /// </summary>
        /// <param name="selected">Selected item</param>
        /// <param name="flag">Determines the item table</param>
        public void DeleteSelected(object selected, string flag)
        {
            string azon = string.Empty;
            Type t = selected.GetType();
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo att in props)
            {
                // itt az attribute neveket majd pontosítani kell ugyannanak kell lennie mint ami a datagridbe van
                if (att.Name == "Email" || att.Name == "Termekazon" || att.Name == "Helyszinazon")
                {
                    azon = att.GetValue(selected, null).ToString();
                }
            }

            if (flag == "U")
            {
                Felhasznalo tmp = this.dBE.Felhasznalo.Where(p => p.Email == azon).FirstOrDefault();
                this.dBE.Felhasznalo.Remove(tmp);
            }
            else if (flag == "P")
            {
                int tmp2 = int.Parse(azon);
                Termek tmp = this.dBE.Termek.Where(p => p.Termekazon == tmp2).FirstOrDefault();
                this.dBE.Termek.Remove(tmp);
            }
            else if (flag == "L")
            {
                int tmp2 = int.Parse(azon);
                Helyszin tmp = this.dBE.Helyszin.Where(p => p.Helyszinazon == tmp2).FirstOrDefault();
                this.dBE.Helyszin.Remove(tmp);
            }

            this.dBE.SaveChanges();
        }

        /// <summary>
        /// Finds the selected item
        /// </summary>
        /// <param name="selected">Selected item</param>
        /// <param name="flag">determines the item table</param>
        /// <returns>Properties</returns>
        public string[] Find(object selected, string flag)
        {
            try
            {
            string azon = string.Empty;
            Type t = selected.GetType();
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo att in props)
            {
                // itt az attribute neveket majd pontosítani kell ugyannanak kell lennie mint ami a datagridbe van
                if ((att.Name == "Email" && flag == "U") || (att.Name == "Termekazon" && flag == "P") || (att.Name == "Helyszinazon" && flag == "L"))
                {
                    azon = att.GetValue(selected, null).ToString();
                }
            }

            if (flag == "U")
            {
                Felhasznalo tmp = this.dBE.Felhasznalo.Where(p => p.Email == azon).FirstOrDefault();
                string[] tomb = new string[]
                {
                    tmp.Nev,
                    tmp.Email,
                    tmp.Jelszo,
                    tmp.Lakcim,
                    tmp.Tipus
                };
                return tomb;
            }
            else if (flag == "P")
            {
                Termek tmp = this.dBE.Termek.Where(p => p.Termekazon.ToString() == azon).FirstOrDefault();
                string[] tomb = new string[]
                {
                    tmp.Termekazon.ToString(),
                    tmp.Nev,
                    tmp.Ar.ToString(),
                    tmp.Helyszinazon.ToString(),
                    tmp.Megjegyzes,
                    tmp.Darabszam.ToString()
                };
                return tomb;
            }
            else if (flag == "L")
            {
                int temp = int.Parse(azon);
                Helyszin tmp = this.dBE.Helyszin.Where(p => p.Helyszinazon == temp).FirstOrDefault();
                string[] tomb = new string[]
                {
                    tmp.Helyszinazon.ToString(),
                    tmp.Varos,
                    tmp.Utca,
                    tmp.Iranyitoszam.ToString(),
                    tmp.Email
                };
                return tomb;
            }
            }
            catch (Exception)
            {
                return null;
            }

            string[] tomb2 = new string[5];
            return tomb2;
        }

        /// <summary>
        /// Decreases the item's quantity
        /// </summary>
        /// <param name="azon">termekazon</param>
        /// <param name="quantity">How much</param>
        public void DecreaseQuantity(int azon, int quantity)
        {
                var tmp = this.dBE.Termek.Where(k => k.Termekazon == azon).FirstOrDefault();
                tmp.Darabszam = tmp.Darabszam - quantity;
                this.dBE.SaveChanges();
        }
    }
}
