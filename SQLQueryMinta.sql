﻿create table Felhasznalo(
Email VARCHAR(40) not null PRIMARY KEY,
Nev VARCHAR(20) not null,
Jelszo VARCHAR(30) not null,
Lakcim VARCHAR(40) not null,
Tipus VARCHAR(20) not null,
Kosarazon numeric(5) not null,
Vegosszeg numeric(7) not null
);

create table Helyszin(
Helyszinazon numeric(7) not null PRIMARY KEY,
Varos VARCHAR(20) not null,
Utca VARCHAR(30) not null,
Iranyitoszam numeric(5) not null,
Email VARCHAR(40) not null,
CONSTRAINT F_Email_fk FOREIGN KEY (Email) REFERENCES Felhasznalo(Email)
);

create table Termek(
Termekazon numeric(5) not null PRIMARY KEY,
Nev VARCHAR(20) not null,
Megjegyzes VARCHAR(100) not null,
Ar numeric(5) not null,
Helyszinazon numeric(7) not null,
CONSTRAINT T_Helyszinazon_fk FOREIGN KEY (Helyszinazon) REFERENCES Helyszin(Helyszinazon)
);

create table Kosar(
Termekazon numeric(5) not null,
Email VARCHAR(40) not null,
CONSTRAINT K_Email_fk FOREIGN KEY (Email) REFERENCES Felhasznalo(Email),
CONSTRAINT K_Termekazon_fk FOREIGN KEY (Termekazon) REFERENCES Termek(Termekazon)
);

create table Log(
Azonosito numeric(5) not null PRIMARY KEY,
Felhasznalo VARCHAR(40) not null,
Uzenet VARCHAR(50) not null,
CONSTRAINT L_Felhasznalo_fk FOREIGN KEY (Felhasznalo) REFERENCES Felhasznalo(Email),
);