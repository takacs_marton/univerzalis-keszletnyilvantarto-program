﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Testing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BusinessLogic;
    using DataManagement;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class containing tests
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private Login login;

        private Logic logic;

        private DB db;

        /// <summary>
        /// Setup for real repo tests
        /// </summary>
        [SetUp]
        public void Setups()
        {
            this.login = new Login();
            this.db = new DB();
            this.logic = new Logic();
        }

        /// <summary>
        /// Tests LoginInto method with Admin
        /// </summary>
        [Test]
        public void Test_Login_Admin()
        {
            var result = this.login.LoginInto("Admin", "Admin");
            Assert.That(result.Email == "Admin");
            Assert.That(result.Lakcim == "Admin");
            Assert.That(result.Nev == "Admin");
            Assert.That(result.Tipus == "Admin");
        }

        /// <summary>
        /// Tests LoginInto method with User
        /// </summary>
        [Test]
        public void Test_Login_User()
        {
            var result = this.login.LoginInto("User", "User");
            Assert.That(result.Email == "User");
            Assert.That(result.Lakcim == "User");
            Assert.That(result.Nev == "User");
            Assert.That(result.Tipus == "Felhasználó");
        }

        /// <summary>
        /// Tests LoginInto method with Transporter
        /// </summary>
        [Test]
        public void Test_Login_Trans()
        {
            var result = this.login.LoginInto("Trans", "Trans");
            Assert.That(result.Email == "Trans");
            Assert.That(result.Lakcim == "Trans");
            Assert.That(result.Nev == "Trans");
            Assert.That(result.Tipus == "Szállító");
        }

        /// <summary>
        /// Tests LoginInto method with errors
        /// </summary>
        [Test]
        public void Test_Login_Error()
        {
            var result = this.login.LoginInto("hiba@teszt.hu", "hiba");
            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests Find method with users
        /// </summary>
        [Test]
        public void Test_Find_User_WithRealRepo()
        {
            string[] expected = new string[5];
            expected[0] = "admin";
            expected[1] = "admin@teszt.hu";
            expected[2] = "123";
            expected[3] = "st";
            expected[4] = "Admin";

            var tmp = new
            {
                Név = "admin",
                Típus = "Admin",
                Email = "admin@teszt.hu",
                Lakcím = "st"
            };

            var result = this.logic.Find(tmp, "U");

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests Find method with products
        /// </summary>
        [Test]
        public void Test_Find_Product_WithRealRepo()
        {
            string[] expected = new string[6];
            expected[0] = "3";
            expected[1] = "Tej";
            expected[2] = "180";
            expected[3] = "1";
            expected[4] = "1,5%";
            expected[5] = "300";

            var tmp = new
            {
                Termekazon = "3",
                Név = "Tej",
                Megjegyzés = "1,5%",
                Ár = "180"
            };

            var result = this.logic.Find(tmp, "P");

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests Find method with places
        /// </summary>
        [Test]
        public void Test_Find_Place_WithRealRepo()
        {
            string[] expected = new string[5];
            expected[0] = "1";
            expected[1] = "Budapest";
            expected[2] = "st";
            expected[3] = "4335";
            expected[4] = "admin@teszt.hu";

            var tmp = new
            {
                Helyszinazon = "1",
                Irányítószám = "4335",
                Város = "Budapest",
                Utca = "st",
                Email = "admin@teszt.hu"
            };

            var result = this.logic.Find(tmp, "L");

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests Find method with wrong flag
        /// </summary>
        [Test]
        public void Test_Find_FlagError_WithRealRepo()
        {
            string[] expected = new string[5];

            var tmp = new
            {
                Azonosító = "1",
                Név = "Tej",
                Megjegyzés = "1 liter",
                Ár = "199"
            };

            var result = this.logic.Find(tmp, "Z");

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests SetTheBuyQuantity method
        /// </summary>
        [Test]
        public void Test_SetTheBuyQuantity_WithRealRepo()
        {
            string[] tmp = new string[6];
            var help = new
            {
                Termekazon = "3",
                Név = "Tej",
                Megjegyzés = "1,5%",
                Ár = "180"
            };
            tmp = this.logic.Find(help, "P");
            Termek newProd = this.logic.SetTheBuyQuantity(tmp, 1000);

            Assert.That(newProd.Darabszam, Is.EqualTo(1000));
        }

        /// <summary>
        /// Tests AddUser method with Mock
        /// </summary>
        [Test]
        public void Test_AddUser_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);
            string[] felh = new string[5];
            felh[0] = "testIDK";
            felh[1] = "test";
            felh[2] = "test1@tests.com";
            felh[3] = "st";
            felh[4] = "Admin";

            var result = logic.AddUser(felh);

            Assert.That(result, Is.True);
            testRepo.Verify(x => x.AddUser(It.IsAny<Felhasznalo>()), Times.Once);
        }

        /// <summary>
        /// Tests AddPlace method with Mock
        /// </summary>
        [Test]
        public void Test_AddPlace_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);
            string[] place = new string[5];
            place[0] = "2";
            place[1] = "Testoville";
            place[2] = "ut";
            place[3] = "1112";
            place[4] = "asd@asd.com";

            var result = logic.AddUser(place);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests AddProduct method with Mock
        /// </summary>
        [Test]
        public void Test_AddProduct_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);
            string[] prod = new string[6];
            prod[0] = "2";
            prod[1] = "testprod";
            prod[2] = "Ez teszt";
            prod[3] = "150";
            prod[4] = "1";
            prod[5] = "1000";

            var result = logic.AddProduct(prod);

            Assert.That(result, Is.True);
            testRepo.Verify(x => x.AddProduct(It.IsAny<Termek>()), Times.Once);
        }

        /// <summary>
        /// Tests AddProduct method with Mock, with Termek property
        /// </summary>
        [Test]
        public void Test_AddProduct_TermekClass_WithRealRepo()
        {
            Termek newprod = new Termek()
            {
                Ar = 200,
                Darabszam = 4000,
                Nev = "Sövényvágó",
                Termekazon = 7,
                Helyszinazon = 1,
                Megjegyzes = "asd"
            };

            this.logic.AddProduct(newprod);

            Assert.That(this.logic.GetAllProducts().ToList().Count, Is.EqualTo(6));
        }

        /// <summary>
        /// Tests DeleteSelected method with Mock, on products
        /// </summary>
        [Test]
        public void Test_DeleteSelected_Product_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var help = logic.GetAllProducts().ToList();
            logic.DeleteSelected(help[0], "P");

            testRepo.Verify(x => x.DeleteSelected(help[0], "P"), Times.Once);
        }

        /// <summary>
        /// Tests DeleteSelected method with Mock, on users
        /// </summary>
        [Test]
        public void Test_DeleteSelected_User_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);
            var help = logic.GetAllUsers().ToList();

            logic.DeleteSelected(help[0], "U");

            testRepo.Verify(x => x.DeleteSelected(help[0], "U"), Times.Once);
        }

        /// <summary>
        /// Tests DeleteSelected method with Mock, on places
        /// </summary>
        [Test]
        public void Test_DeleteSelected_Place_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var help = logic.GetAllPlaces().ToList();
            logic.DeleteSelected(help[0], "L");

            testRepo.Verify(x => x.DeleteSelected(help[0], "L"), Times.Once);
        }

        /// <summary>
        /// Tests GetTempProductsList method
        /// </summary>
        [Test]
        public void Test_GetTempProductlist_WithRealRepo()
        {
            var result = this.logic.GetTempProductsList();

            Assert.That(result[0].Nev, Is.EqualTo("Krumpli"));
            Assert.That(result[0].Termekazon, Is.EqualTo(1));
            Assert.That(result[0].Helyszinazon, Is.EqualTo(1));
            Assert.That(result[1].Nev, Is.EqualTo("Kenyér"));
            Assert.That(result[1].Termekazon, Is.EqualTo(2));
            Assert.That(result[1].Helyszinazon, Is.EqualTo(1));
            Assert.That(result[2].Nev, Is.EqualTo("Tej"));
            Assert.That(result[2].Termekazon, Is.EqualTo(3));
            Assert.That(result[2].Helyszinazon, Is.EqualTo(1));
            Assert.That(result[3].Nev, Is.EqualTo("Ásvány víz"));
            Assert.That(result[3].Termekazon, Is.EqualTo(4));
            Assert.That(result[3].Helyszinazon, Is.EqualTo(1));
            Assert.That(result[4].Nev, Is.EqualTo("Sonka"));
            Assert.That(result[4].Termekazon, Is.EqualTo(5));
            Assert.That(result[4].Helyszinazon, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests DeleteSelected method with Mock, with flag error
        /// </summary>
        [Test]
        public void Test_DeleteSelected_FlagError_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var help = logic.GetAllProducts().ToList();
            logic.DeleteSelected(help[0], "L");

            testRepo.Verify(x => x.DeleteSelected(help[0], "L"), Times.Once);
            Assert.That(logic.GetAllPlaces().ToList().Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Tests GetAllUsers method with Mock
        /// </summary>
        [Test]
        public void Test_GetAllUsers_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var result = logic.GetAllUsers();

            Assert.That(result.Count(), Is.EqualTo(2));
            testRepo.Verify(x => x.GetAllUsers(), Times.Once());
        }

        /// <summary>
        /// Tests GetAllPlaces method with Mock
        /// </summary>
        [Test]
        public void Test_GetAllPlaces_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var result = logic.GetAllPlaces();

            Assert.That(result.Count(), Is.EqualTo(2));
            testRepo.Verify(x => x.GetAllPlaces(), Times.Once());
        }

        /// <summary>
        /// Tests DecreaseQuantity method with Mock
        /// </summary>
        [Test]
        public void Test_DecreaseQuantity_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            logic.DecreaseQuantity(1, 50);

            testRepo.Verify(x => x.DecreaseQuantity(1, 50), Times.Once);
        }

        /// <summary>
        /// Tests GetAllProducts method with Mock
        /// </summary>
        [Test]
        public void Test_GetAllProducts_WithMockRepo()
        {
            Mock<IDbRepo> testRepo = this.CreateMockRepo();
            ILogic logic = new Logic(testRepo.Object);

            var result = logic.GetAllProducts();

            Assert.That(result.Count(), Is.EqualTo(2));
            testRepo.Verify(x => x.GetAllProducts(), Times.Once());
        }

        /// <summary>
        /// Tests GetSpecificProducts method
        /// </summary>
        [Test]
        public void Test_GetSpecificProducts_WithRealRepo()
        {
            List<TempProduct> result = this.logic.GetSpecificProducts("Tej");

            Assert.That(result[0].Nev, Is.EqualTo("Tej"));
            Assert.That(result[0].Helyszinazon, Is.EqualTo(1));
            Assert.That(result[0].Termekazon, Is.EqualTo(3));
        }

        /// <summary>
        /// Tests GetSpecificProducts method with errors
        /// </summary>
        [Test]
        public void Test_GetSpecificProducts_Error_WithRealRepo()
        {
            List<TempProduct> result = this.logic.GetSpecificProducts("Lap");

            Assert.That(result, Is.Empty);
        }

        private Mock<IDbRepo> CreateMockRepo()
        {
            Mock<IDbRepo> testRepositoryMock = new Mock<IDbRepo>();

            List<Felhasznalo> testUsers = new List<Felhasznalo>();
            testUsers.Add(new Felhasznalo()
            {
                Email = "asd@asd.com",
                Nev = "TestAdmin",
                Jelszo = "test",
                Lakcim = "st",
                Tipus = "Admin",
                Kosarazon = 0,
                Vegosszeg = 0
            });
            testUsers.Add(new Felhasznalo()
            {
                Email = "asd1@asd.com",
                Nev = "TestUser",
                Jelszo = "test",
                Lakcim = "st",
                Tipus = "Felhasznalo",
                Kosarazon = 1,
                Vegosszeg = 0
            });
            testRepositoryMock.Setup(m => m.GetAllUsers()).Returns(testUsers.AsQueryable());

            List<Helyszin> testPlaces = new List<Helyszin>();
            testPlaces.Add(new Helyszin()
            {
                Email = "asd@asd.com",
                Iranyitoszam = 123,
                Utca = "st",
                Varos = "Budapest",
                Felhasznalo = new Felhasznalo()
                {
                    Email = "asd@asd.com",
                    Nev = "TestAdmin",
                    Jelszo = "test",
                    Lakcim = "st",
                    Tipus = "Admin",
                    Kosarazon = 0,
                    Vegosszeg = 0
                },
                Helyszinazon = 0
            });
            testPlaces.Add(new Helyszin()
            {
                Email = "asd@asd.com",
                Iranyitoszam = 4344,
                Utca = "st",
                Varos = "Sopron",
                Helyszinazon = 1,
                Felhasznalo = new Felhasznalo()
                {
                    Email = "asd@asd.com",
                    Nev = "TestAdmin",
                    Jelszo = "test",
                    Lakcim = "st",
                    Tipus = "Admin",
                    Kosarazon = 0,
                    Vegosszeg = 0
                }
            });
            testRepositoryMock.Setup(m => m.GetAllPlaces()).Returns(testPlaces.AsQueryable());

            List<Termek> testProducts = new List<Termek>();
            testProducts.Add(new Termek()
            {
                Nev = "Asztal",
                Megjegyzes = "Bükkfa",
                Helyszinazon = 1,
                Termekazon = 0,
                Ar = 200,
                Darabszam = 1000
            });
            testProducts.Add(new Termek()
            {
                Nev = "Trolibusz",
                Megjegyzes = "Olcsó",
                Helyszinazon = 0,
                Termekazon = 1,
                Ar = 300,
                Darabszam = 500
            });
            testRepositoryMock.Setup(m => m.GetAllProducts()).Returns(testProducts.AsQueryable());

            return testRepositoryMock;
        }
    }
}
