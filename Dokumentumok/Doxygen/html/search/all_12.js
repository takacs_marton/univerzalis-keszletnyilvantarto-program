var searchData=
[
  ['saveboughtproducts',['SaveBoughtProducts',['../interface_business_logic_1_1_i_logic.html#ab0acfa74dbea41841ae61385b18b5330',1,'BusinessLogic.ILogic.SaveBoughtProducts()'],['../class_business_logic_1_1_logic.html#a554793b56d9b9eb8688ec0696d24a961',1,'BusinessLogic.Logic.SaveBoughtProducts()']]],
  ['securepasswordhasher',['SecurePasswordHasher',['../class_business_logic_1_1_secure_password_hasher.html',1,'BusinessLogic']]],
  ['selectedbuyproduct',['SelectedBuyProduct',['../class_feleves_feladat_1_1_user_shopping_view_model.html#a1994b93b03f5403c8a48ce00a6c10d08',1,'FelevesFeladat::UserShoppingViewModel']]],
  ['selectedproduct',['SelectedProduct',['../class_feleves_feladat_1_1_user_shopping_view_model.html#aa5fef1a5d40e4bc3d1b0f15c43b2f214',1,'FelevesFeladat::UserShoppingViewModel']]],
  ['setthebuyquantity',['SetTheBuyQuantity',['../interface_business_logic_1_1_i_logic.html#ad2bb8783adf20469e3896c65cc34faa0',1,'BusinessLogic.ILogic.SetTheBuyQuantity()'],['../class_business_logic_1_1_logic.html#a6d4df51ee2aaf98363e40ad2e974590b',1,'BusinessLogic.Logic.SetTheBuyQuantity()']]],
  ['setups',['Setups',['../class_testing_1_1_tests.html#ae8f4bc28070ccede0eec9a1d12c0f965',1,'Testing::Tests']]],
  ['stock_5fwindows',['Stock_windows',['../class_feleves_feladat_1_1_stock__windows.html',1,'FelevesFeladat.Stock_windows'],['../class_feleves_feladat_1_1_stock__windows.html#a447a26bbeac33f6fdcdca20dbf7b212b',1,'FelevesFeladat.Stock_windows.Stock_windows()']]]
];
