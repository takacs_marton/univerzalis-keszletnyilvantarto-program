var searchData=
[
  ['dbentities',['dbEntities',['../class_data_management_1_1db_entities.html#a0af7afca593d1b13a7c6292f04fda67c',1,'DataManagement::dbEntities']]],
  ['dbrepo',['DbRepo',['../class_data_management_1_1_db_repo.html#a5d778083f42b2ce954432aebe017f621',1,'DataManagement::DbRepo']]],
  ['decreasequantity',['DecreaseQuantity',['../interface_business_logic_1_1_i_logic.html#a5ec5527360a7a85b6fe3d21e3c59b09c',1,'BusinessLogic.ILogic.DecreaseQuantity()'],['../class_business_logic_1_1_logic.html#ab84c064278ddee2229607f9c219530e3',1,'BusinessLogic.Logic.DecreaseQuantity()'],['../class_data_management_1_1_db_repo.html#aebe2bc8f3afae922a36c06e1e90272bf',1,'DataManagement.DbRepo.DecreaseQuantity()'],['../interface_data_management_1_1_i_db_repo.html#adce9177b9ededc23158528a637ec776d',1,'DataManagement.IDbRepo.DecreaseQuantity()']]],
  ['deleteselected',['DeleteSelected',['../interface_business_logic_1_1_i_logic.html#a9fb9aff09dbfc78ea7e5b9f313f4575f',1,'BusinessLogic.ILogic.DeleteSelected()'],['../class_business_logic_1_1_logic.html#ad5d7a4701e762d537fbd015f8457fbe4',1,'BusinessLogic.Logic.DeleteSelected()'],['../class_data_management_1_1_db_repo.html#a879ef39371610c34121c93c50962a59e',1,'DataManagement.DbRepo.DeleteSelected()'],['../interface_data_management_1_1_i_db_repo.html#a92e4bfca3a4626ec1d5c4bf1e694c854',1,'DataManagement.IDbRepo.DeleteSelected()']]]
];
