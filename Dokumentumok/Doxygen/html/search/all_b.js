var searchData=
[
  ['lakcim',['Lakcim',['../class_data_management_1_1_felhasznalo.html#a91874a83d435dd03ba2f5253e037bf3f',1,'DataManagement::Felhasznalo']]],
  ['locationedit',['LocationEdit',['../class_feleves_feladat_1_1_location_edit.html',1,'FelevesFeladat.LocationEdit'],['../class_feleves_feladat_1_1_location_edit.html#a98ef465ef4a541d5ff3e6c71e12c3736',1,'FelevesFeladat.LocationEdit.LocationEdit()']]],
  ['log',['Log',['../class_data_management_1_1_log.html',1,'DataManagement.Log'],['../class_data_management_1_1db_entities.html#a47339f1a320988fa7f7464f1c57703c9',1,'DataManagement.dbEntities.Log()']]],
  ['logcreator',['LogCreator',['../interface_business_logic_1_1_i_logic.html#a52c00a3ff1a1c540ae48735570558f25',1,'BusinessLogic.ILogic.LogCreator()'],['../class_business_logic_1_1_logic.html#ad773967ae7302c417d835383b5dad988',1,'BusinessLogic.Logic.LogCreator()']]],
  ['logic',['Logic',['../class_business_logic_1_1_logic.html',1,'BusinessLogic.Logic'],['../class_business_logic_1_1_logic.html#aa7333ee6a7612de3a8449affb3786be0',1,'BusinessLogic.Logic.Logic(IDbRepo dbrepo)'],['../class_business_logic_1_1_logic.html#a81c74e57f7d10273754467cd25afe5d0',1,'BusinessLogic.Logic.Logic()']]],
  ['login',['Login',['../class_business_logic_1_1_login.html',1,'BusinessLogic.Login'],['../class_business_logic_1_1_login.html#a70d39bfb8ea4a2132afcac43456c0cdd',1,'BusinessLogic.Login.Login()']]],
  ['logininto',['LoginInto',['../class_business_logic_1_1_login.html#aefcc5157e44e33201f2f3e2f9f85d939',1,'BusinessLogic::Login']]],
  ['logwindow',['LogWindow',['../class_feleves_feladat_1_1_log_window.html',1,'FelevesFeladat.LogWindow'],['../class_feleves_feladat_1_1_log_window.html#aa0bdb3691166d3545152eec077418c53',1,'FelevesFeladat.LogWindow.LogWindow()']]]
];
